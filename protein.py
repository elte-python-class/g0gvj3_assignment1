from functools import total_ordering

## there are 20 amino acids who make the protein
## sequence
acid_names = [
    "A",
    "G",
    "M",
    "L",
    "H",
    "R",
    "N",
    "D",
    "T",
    "F",
    "W",
    "K",
    "Q",
    "E",
    "S",
    "P",
    "V",
    "C",
    "I",
    "Y",
]


## lets us load the fasta file and read the
## contents


def load_fasta(path):

    fasta = open(path).read()
    # fasta file is a string
    # split the file along >sp
    fasta = fasta.split(">sp")
    #fasta = fasta[1:]

    protein_list = []
    try:
        fasta.pop(0)
        for proteins in fasta:
            protein_list.append(Protein(proteins))

        return protein_list
    except IndexError:
        raise ValueError("There are no proteins in the proteins.fasta file")


## using total_ordering
@total_ordering
class Protein(list):
    def __init__(self, fasta):
        first_line = fasta.split("\n")[0]

        seq = fasta.split("\n")[1 : len(fasta.split("\n")) - 1]

        self.sequence = "".join(seq)
        self.size = len(self.sequence)

        self.id = first_line.split("|")[1]
        fls = first_line.split("|")[2]

        os_ind = fls.find("OS=")
        ox_ind = fls.find("OX=")
        pe_ind = fls.find("PE=")
        sv_ind = fls.find("SV=")
        gn_ind = fls.find("GN=")

        self.name = fls[:os_ind].strip()
        self.os = fls[os_ind + 3 : ox_ind]
        self.gn = fls[gn_ind + 3 : pe_ind].strip()
        self.ox = int(fls[ox_ind + 3 : gn_ind])
        self.pe = int(fls[pe_ind + 3 : sv_ind])
        self.sv = int(fls[sv_ind + 3 :])

        self.aminoacidcounts = {}

        for acids in acid_names:
            self.aminoacidcounts[acids] = self.sequence.count(acids)
        super().__init__(self.sequence)

    def __eq__(self, other):
        return self.size == other.size

    def __lt__(self, other):
        return self.size < other.size

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return "{} id: {}".format(self.name, self.id)


def sort_proteins_pe(proteins):
    return sorted(proteins, key=lambda prot: prot.pe, reverse=True)


def sort_proteins_aa(proteins, acid):
    if acid not in acid_names:
        raise ValueError("not an aminoacid")
    return sorted(proteins, key=lambda prot: prot.aminoacidcounts[acid], reverse=True)


def find_protein_with_motif(proteins, motif):

    protein_by_motif = []
    for prot in proteins:
        if prot.sequence.find(motif) != -1:
            protein_by_motif.append(prot)

    return protein_by_motif

